import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';


import { AppComponent } from './app.component';
import { HomeComponent } from './componets/home/home.component';
import { FooterComponent } from './componets/footer/footer.component';
import { NavbarComponent } from './componets/navbar/navbar.component';

/* === services === */
import { BreedService } from './services/breed/breed.service';
import { NoteService } from './services/note/note.service';
import { LogService } from './services/log/log.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FooterComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    HttpModule
  ],
  providers: [
    BreedService,
    NoteService,
    LogService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
