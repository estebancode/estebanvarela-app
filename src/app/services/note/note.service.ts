import { Injectable } from '@angular/core';
import { RequestOptions, Http, Headers,Response} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { NoteModel } from '../../models/note.model';

@Injectable()
export class NoteService {

  constructor(private _http:Http) { }

  Create(note:NoteModel){
    let endPoint = 'http://localhost:5555/notes/';
    return this._http.post(endPoint,note).map((resp:Response)=>
      resp.json()
    );
  }

  GetAllBy(breed){
    let endPoint = 'http://localhost:5555/notes?Breed='+breed;
    return this._http.get(endPoint).map(resp=>
      resp.json()
    );
  }

}
