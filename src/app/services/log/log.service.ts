import { Injectable } from '@angular/core';
import { RequestOptions, Http, Headers,Response} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { LogModel } from '../../models/log.model';

@Injectable()
export class LogService {

  constructor(private _http:Http) { }

  Create(log:LogModel){
    let endPoint = 'http://localhost:6565/logs/';
    return this._http.post(endPoint,log).map((resp:Response)=>
      resp.json()
    );
  }

  GetAllBy(breed){
    let endPoint = 'http://localhost:6565/logs?Breed='+breed;
    return this._http.get(endPoint).map(resp=>
      resp.json()
    );
  }

}
