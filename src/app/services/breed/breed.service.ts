import { Injectable } from '@angular/core';
import { RequestOptions, Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class BreedService {

  private urlBase = 'https://dog.ceo/api/breeds/list/all';
  private options: RequestOptions;
  constructor(private _http: Http) {
  }

  
  GetAll() {
      let endPoint = `${this.urlBase}`;
      return this._http.get(this.urlBase).map(resp=>
        resp.json()
      );
  }

  GetByName(breed){
    let endPoint = 'https://dog.ceo/api/breed/'+breed+'/images';
    return this._http.get(endPoint).map(resp=>
      resp.json()
    );
  }

  GetBySubBreed(breed){
    let endPoint = 'https://dog.ceo/api/breed/'+breed+'/list';
    return this._http.get(endPoint).map(resp=>
      resp.json()
    );
  }

  GetBySubBreedImage(breed,subBredd){
    let endPoint = 'https://dog.ceo/api/breed/'+breed+'/'+subBredd+'/images';
    return this._http.get(endPoint).map(resp=>
      resp.json()
    );
  }

}
