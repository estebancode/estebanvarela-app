import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

/* === services === */
import { BreedService } from '../../services/breed/breed.service';
import { NoteService } from '../../services/note/note.service';
import { LogService } from '../../services/log/log.service';

/* === Models === */
import { SubBreedModel } from '../../models/subbreed.model';
import { NoteModel } from '../../models/note.model';
import { LogModel } from '../../models/log.model';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private _breedService: BreedService, private _noteService: NoteService, private _logService: LogService) { }

  breeds: any = [];
  breed: any = [];
  subBreeds: SubBreedModel[] = [];
  viewBreed: Boolean = false;
  note: String = '';
  notes: Array<NoteModel> = [];
  breedSeleted: String = '';
  logs: LogModel[] = [];
  viewLogs = false;

  ngOnInit() {
    this.LoadBreeds();
  }

  LoadBreeds() {
    this._breedService.GetAll().subscribe(
      data => {
        this.breeds = Object.keys(data.message);
      }
    );
  }

  ChangeBreed(breed) {

    this.breed = [];
    this.subBreeds = [];
    this.notes = [];
    this.viewBreed = false;
    let list = [];
    this.logs = [];
    this.breedSeleted = breed;
    this.viewLogs = false;

    this.GetAllNoteBy(breed);

    this._breedService.GetByName(breed).subscribe(
      data => {
        this.breed = data.message;
      }
    );

    this._breedService.GetBySubBreed(breed).subscribe(
      data => {
        list = data.message;
        for (let entry of list) {
          this._breedService.GetBySubBreedImage(breed, entry).subscribe(
            data => {
              let item = data.message;
              let subbreedmodel = new SubBreedModel();
              subbreedmodel.Name = entry;
              subbreedmodel.ImageURL = item;
              this.subBreeds.push(subbreedmodel);
            }
          );
        }
        this.GetAllByLog(breed);
        this.viewBreed = true;
      }
    );
  }

  AddNewNote(note, breed) {
    let noteCreate = new NoteModel();
    noteCreate.Note = note,
      noteCreate.Breed = breed;
    this._noteService.Create(noteCreate).subscribe(
      data => {
        this.GetAllNoteBy(breed);
        this.AddNewLog(noteCreate);
      }
    );
  }

  GetAllNoteBy(breed) {
    this._noteService.GetAllBy(breed).subscribe(
      data => {
        this.notes = data;
      }
    );
  }

  AddNewLog(note: NoteModel) {
    let log = new LogModel();
    log.Breed = note.Breed;
    log.Note = note.Note,
      log.CreationDate = new Date();
    this._logService.Create(log).subscribe(
      data => {
        console.log(data);
      }
    );
  }

  GetAllByLog(breed) {
    this._logService.GetAllBy(breed).subscribe(
      data => {
        this.logs = data;
        console.log('========== star LOG of' + breed + ' ===============');
        for (let item of data) {
          console.log(item);
        }
        console.log('========== end LOG of' + breed + ' ================');
        if (data.length > 0) {
          this.viewLogs = true;
        }
        setTimeout(() => {
          this.viewLogs = false;
        }, 3000);
      }
    );
  }

}
